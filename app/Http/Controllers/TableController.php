<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Table;
use Response;

class TableController extends Controller
{
    // ajax return
    public function ajax(){
        $table = Table::orderBy('id', 'desc')->get();
        return Response::json([
            'data' => $table
        ], 200);
    }

    // table return view
    public function index() {
        return view('table');
    }

    // table information store
    public function store(Request $request){
        $table = new Table;
        $table -> firstName = $request -> firstName;
        $table -> lastName = $request -> lastName;
        $table -> save();
        return $table;
    }

    // delete
    public function delete($id){
        $table = Table::find($id)->delete();
        return $table;
    }

    // edit
    public function edit($id, Request $request){
        $table = Table::where('id', $id)->first();
        $table -> firstName = $request -> firstName;
        $table -> lastName = $request -> lastName;
        $table -> save();
        return $table;
    }
    public function allDelete($id, Request $request)
    {
        $all_id = explode(',' , $id);
        foreach($all_id as $as){
            $data = Table::where('id', $as)->delete();
        }
        return $data;
    }
}

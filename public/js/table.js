
var data;
var limit = 1;
var table = document.getElementById('forTable');
var search = document.getElementById('search');
// -------Form----------
var form = document.getElementById('form');
var editForm = document.getElementById('editFrom');

var editName = document.getElementById('editName');
var editSurname = document.getElementById('editSurname');
var deleteText = document.getElementById('deleteText');
// -------Button----------
var addSubmit = document.getElementById('submit');
var editBtn = document.getElementById('editBtn');
var deleteBtn = document.getElementById('deleteBtn');
var deleteCheck;
var allDeleteBtn = document.getElementById('allDeleteBtn');
// -----------input form----------
var names = document.getElementById('firstName');
var surname = document.getElementById('lastName');
// ------------http request---------
const http = new XMLHttpRequest();
// delete check
var deletes=[];
var delete_users = [];

// line
function line(info){
    var limit = 1;
    information = info.map(element => `
    <tr class="tr-table" id="sortable">
        <td class="check">
            <input  type="checkbox" class="deleteCheck" value="`+element.id+`" name="checkName">
        </td>
        <td>`+(limit++)+`</td>
        <td>`+element.firstName+`</td>
        <td>`+element.lastName+`</td>
        <td>`+element.created_at.substring(0, 10) +' '+ element.created_at.substring(11, 16)+`</td>
        <td class="action">
            <button class="act-btn" data-bs-toggle="modal" data-bs-target="#exampleModalDelete" onclick="deleteForm(`+element.id+`)">
                <i class="bx bx-trash"></i>
            </button>
            <button class="act-btn" data-bs-toggle="modal" data-bs-target="#exampleModalEdit" onclick="editFromText(`+element.id+`)">
                <i class="bx bx-edit"></i>
            </button>
        </td>
    </tr>
    `)
    information = information.join('')
    table.innerHTML = information

    // checkbox delete function
    checkboxDelete()
}

// onload
function open(){
    http.onload = function() {
        data = JSON.parse(this.response)
        data = data.data
        line(data)
        }
        http.open('GET', 'http://127.0.0.1:8000/ajax')
        http.send()
}

// windows 
window.addEventListener("DOMContentLoaded", function(){
    open()
})

// search
search.addEventListener('keyup', function() {
    let filter = search.value.toUpperCase();
    d = data.filter(function (val){
        if( val.firstName.toUpperCase().indexOf(filter) > -1 || val.lastName.toUpperCase().indexOf(filter) > -1)
        return val;
    });
    line(d)
})


// ------------Store add-----------
addSubmit.addEventListener('click', function(){
    const http = new XMLHttpRequest();
    var formData = new FormData(form)
    http.open('POST', 'http://127.0.0.1:8000/tableUpdate')
    http.send(formData)
    open()
    
    if(names.value.length>0 && surname.value.length>0) {
        names.placeholder = 'Success';
        surname.placeholder = 'Success';
        names.value = '';
        surname.value = '';
    }else{
        names.placeholder = 'Enter a reference';
        surname.placeholder = 'Enter a reference';
    }
})


// ------------Edit information------------
function editFromText(val){
    var infoEdit = this.data.filter(a => a.id == val)[0];
    editName.value = infoEdit.firstName;
    editSurname.value = infoEdit.lastName;
    editId = infoEdit.id;
}

editBtn.addEventListener('click', function(){
    const http = new XMLHttpRequest();
    var formEdit = new FormData(editForm)
    http.open('POST', 'http://127.0.0.1:8000/tableEdit/'+editId)
    http.send(formEdit)
    open()
})

// ============DELETE===============

// -----------Delete all checkbox-----------
function checkboxDelete(){
    this.deleteCheck = document.querySelectorAll('.deleteCheck');
    deleteCheck.forEach(function(checkbox) {
        checkbox.addEventListener('change', function() {
            if(checkbox.checked == true){
                let user = data
                deletes.push(checkbox.value)
                let delete_filter = user.filter(a => a.id == checkbox.value)
                if(delete_users.filter(v => v.id == checkbox.value).length < 1){
                    delete_users.push(delete_filter[0])
                }
                allDeleteBtn.style.display = 'block';
            }
            else{
                deletes = deletes.filter(e => {
                    if(e != checkbox.value)
                    return e
                })
                // check remove 
                delete_users = delete_users.filter(remove => {
                    if(remove.id != checkbox.value){
                        return remove
                    }
                })
                if(deletes.length == 0){
                    allDeleteBtn.style.display = 'none';
                }
            }
            
        })
      })
}

// ---------One delete function show--------
function deleteForm(val){
    var del_filter = data.filter(a=>a.id == val);
    var delete_show = del_filter.map(a => `
        <div style="font-weight: 700">Full name: `+a.firstName+' '+a.lastName+`</div>    
    `)
    delete_show.join('')
    deleteText.innerHTML = delete_show
    one_id = val;
    deleteBtn.innerText = 'Delete';
}

//   --------all delete function show----------
var btnal;
allDeleteBtn.addEventListener('click', function(){
    allDeleteFunc()
    btnal = deleteBtn.innerText = 'Deletes';
})

function allDeleteFunc(){
    var deleteData = delete_users.map(a => `
        <div style="font-weight: 700">Full name: `+a.firstName+' '+a.lastName+`</div>    
    `)
    deleteData.join('')
    deleteText.innerHTML = deleteData
}

// delete btn get
deleteBtn.addEventListener('click', function(){
    let id = deletes;
    deletes=[];
    delete_users = [];
    const http = new XMLHttpRequest();
    if(btnal == 'Deletes'){
        http.open('GET', 'http://127.0.0.1:8000/allDelete/'+id)
    }else{
        http.open('GET', 'http://127.0.0.1:8000/deleteTable/'+one_id)
    }
    http.send()
    open()
    allDeleteBtn.style.display = 'none';
})


// ----------delete all check----------
function toggle(source) {
    checkboxes = document.getElementsByName('checkName');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }

// Sortable
const sortable = document.getElementById('forTable');
    new Sortable(sortable, {
        Animation: 350,
        ghostClass: 'sortable'
    });

@extends('layouts.app')

@section('content')

    <h3 class="text-center">Students tabel</h3>
    <hr>
    <div class="nav-table">
        <form id="form" method="POST">
            @csrf
            <input class="input" type="text" placeholder="Your name" name="firstName" id="firstName" required>
            <input class="input" type="text" placeholder="Your surname" name="lastName" id="lastName" required>
            <button type="button" class="btn btn-success" id="submit">Submit</button>
        </form>
    </div>
        <div class="nav-body bg-slate">
            <table class="table">
                <thead>
                    <div class="table-serach">
                        <h3>Table created using ajax</h3>
                        <input class="input" type="text" placeholder="Enter the search ..." id="search">
                    </div>
                    <tr class="table-th">
                        <th scope="col" class="width-check">
                            <input type="checkbox" id="allDeleteCheck" onClick="toggle(this)">
                        </th>
                        <th scope="col" class="width">No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th> 
                        <th scope="col">Created</th> 
                        <th scope="col" class="width">Action</th>
                    </tr>
                </thead>
                <tbody id="forTable" class="tbody">
                    {{-- Table ajax return --}}
                </tbody>
            </table>
            <div class="table-footer">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                      <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#">Next</a></li>
                    </ul>
                  </nav>
                <button type="button" class="btn btn-danger" id="allDeleteBtn" data-bs-toggle="modal" data-bs-target="#exampleModalDelete">Delete All</button>
            </div>
        </div>
@endsection
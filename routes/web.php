<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Table
Route::get('/table', [TableController::class, 'index']); //table index
Route::post('/tableUpdate', [TableController::class, 'store']); //table update information
Route::get('/ajax', [TableController::class, 'ajax']);//ajax all post
Route::get('/deleteTable/{id}', [TableController::class, 'delete']);//delete post
Route::post('/tableEdit/{id}', [TableController::class, 'edit']); //table edit information
Route::get('/allDelete/{id}', [TableController::class, 'allDelete']);//delete all checked

